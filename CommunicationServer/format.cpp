#include "format.h"

namespace cs {
namespace utility {

    std::string Format::Format2Hex( const char *begin, size_t len, int interval /*= 0*/, char interval_symbol /*= ' '*/, int line_len /*= 0*/ )
    {
        if (!begin)
            return "";

        std::stringstream ss;
        for (size_t i = 0; i < len; i++)
        {
            ss << std::hex << std::right << std::setfill('0') << std::uppercase << std::setw(2);
            ss << ((int)begin[i]);
  
            if (interval != 0 && ((i + 1) % interval) == 0)
                ss << interval_symbol;
  
            if (line_len != 0 && ((i + 1) % line_len) == 0)
                ss << '\n';
        }
        return ss.str();
    }

}
}