#ifndef CS_BASE_ANY_TYPE_H
#define CS_BASE_ANY_TYPE_H
#include <stdint.h>
#include <ctime>
#include <string>
#include <boost/any.hpp>
#include <glog/logging.h>

namespace cs
{
    enum ItemType
    {
        IT_INT8_T,
        IT_UINT8_T,
        IT_INT16_T,
        IT_UINT16_T,
        IT_INT32_T,
        IT_UINT32_T,
        IT_INT64_T,
        IT_UINT64_T,
        IT_DOUBLE_T,
        IT_STRING,
        IT_DATETIME,
        IT_OTHER,
        IT_UNKNOWN
    };

    class AnyType
    {
    public:
        AnyType () : it(IT_UNKNOWN) {}

        virtual ~AnyType(void);

        inline void set_value(const boost::any &a, ItemType item_type)
        {
            own = a;
            it = item_type;
        }

        inline ItemType get_type() const
        {
            return it;
        }

        bool get_value(const std::string &str_type, void *buf, size_t len);

        template<typename T>
        T get() const
        {
            T tmp;
            get(tmp);
            return tmp;
        }

        template<typename T>
        T get(T &rs) const
        {
            if (IT_UNKNOWN == it)
                return rs;

            try
            {
                rs = boost::any_cast<T>(own);
            }
            catch(boost::bad_any_cast &e)
            {
                LOG(ERROR) << "any_type cast error:" << e.what();
            }

            return rs;
        }

        template<>
        int8_t get(int8_t &d) const
        {
            int8_t i8_;
            uint8_t u8_;
            int16_t i16_;
            uint16_t u16_;
            int32_t i32_;
            uint32_t u32_;
            int64_t i64_;
            uint64_t u64_;
            double dble_;
            std::tm tm_;
            time_t tmp;

            switch (it)
            {
            case IT_INT8_T:
                i8_ = boost::any_cast<int8_t>(own);
                d = (int8_t)i8_;
                break;
            case IT_UINT8_T:
                u8_ = boost::any_cast<uint8_t>(own);
                d = (int8_t)u8_;
                break;
            case IT_INT16_T:
                i16_ = boost::any_cast<int16_t>(own);
                d = (int8_t)i16_;
                break;
            case IT_UINT16_T:
                u16_ = boost::any_cast<uint16_t>(own);
                d = (int8_t)u16_;
                break;
            case IT_INT32_T:
                i32_ = boost::any_cast<int32_t>(own);
                d = (int8_t)i32_;
                break;
            case IT_UINT32_T:
                u32_ = boost::any_cast<uint32_t>(own);
                d = (int8_t)u32_;
                break;
            case IT_INT64_T:
                i64_ = boost::any_cast<int64_t>(own);
                d = (int8_t)i64_;
                break;
            case IT_UINT64_T:
                u64_ = boost::any_cast<uint64_t>(own);
                d = (int8_t)u64_;
                break;
            case IT_DOUBLE_T:
                dble_ = boost::any_cast<double>(own);
                d = (int8_t)dble_;
                break;
            case IT_DATETIME:
                tm_ = boost::any_cast<std::tm>(own);
                tmp = std::mktime(&tm_);
                d = (int8_t)tmp;
                break;
            }

            return d;
        }

        template<>
        uint8_t get(uint8_t &d) const
        {
            int8_t i8_;
            uint8_t u8_;
            int16_t i16_;
            uint16_t u16_;
            int32_t i32_;
            uint32_t u32_;
            int64_t i64_;
            uint64_t u64_;
            double dble_;
            std::tm tm_;
            time_t tmp;

            switch (it)
            {
            case IT_INT8_T:
                i8_ = boost::any_cast<int8_t>(own);
                d = (uint8_t)i8_;
                break;
            case IT_UINT8_T:
                u8_ = boost::any_cast<uint8_t>(own);
                d = (uint8_t)u8_;
                break;
            case IT_INT16_T:
                i16_ = boost::any_cast<int16_t>(own);
                d = (uint8_t)i16_;
                break;
            case IT_UINT16_T:
                u16_ = boost::any_cast<uint16_t>(own);
                d = (uint8_t)u16_;
                break;
            case IT_INT32_T:
                i32_ = boost::any_cast<int32_t>(own);
                d = (uint8_t)i32_;
                break;
            case IT_UINT32_T:
                u32_ = boost::any_cast<uint32_t>(own);
                d = (uint8_t)u32_;
                break;
            case IT_INT64_T:
                i64_ = boost::any_cast<int64_t>(own);
                d = (uint8_t)i64_;
                break;
            case IT_UINT64_T:
                u64_ = boost::any_cast<uint64_t>(own);
                d = (uint8_t)u64_;
                break;
            case IT_DOUBLE_T:
                dble_ = boost::any_cast<double>(own);
                d = (uint8_t)dble_;
                break;
            case IT_DATETIME:
                tm_ = boost::any_cast<std::tm>(own);
                tmp = std::mktime(&tm_);
                d = (uint8_t)tmp;
                break;
            }

            return d;
        }

        template<>
        int16_t get(int16_t &d) const
        {
            int8_t i8_;
            uint8_t u8_;
            int16_t i16_;
            uint16_t u16_;
            int32_t i32_;
            uint32_t u32_;
            int64_t i64_;
            uint64_t u64_;
            double dble_;
            std::tm tm_;
            time_t tmp;

            switch (it)
            {
            case IT_INT8_T:
                i8_ = boost::any_cast<int8_t>(own);
                d = (int16_t)i8_;
                break;
            case IT_UINT8_T:
                u8_ = boost::any_cast<uint8_t>(own);
                d = (int16_t)u8_;
                break;
            case IT_INT16_T:
                i16_ = boost::any_cast<int16_t>(own);
                d = (int16_t)i16_;
                break;
            case IT_UINT16_T:
                u16_ = boost::any_cast<uint16_t>(own);
                d = (int16_t)u16_;
                break;
            case IT_INT32_T:
                i32_ = boost::any_cast<int32_t>(own);
                d = (int16_t)i32_;
                break;
            case IT_UINT32_T:
                u32_ = boost::any_cast<uint32_t>(own);
                d = (int16_t)u32_;
                break;
            case IT_INT64_T:
                i64_ = boost::any_cast<int64_t>(own);
                d = (int16_t)i64_;
                break;
            case IT_UINT64_T:
                u64_ = boost::any_cast<uint64_t>(own);
                d = (int16_t)u64_;
                break;
            case IT_DOUBLE_T:
                dble_ = boost::any_cast<double>(own);
                d = (int16_t)dble_;
                break;
            case IT_DATETIME:
                tm_ = boost::any_cast<std::tm>(own);
                tmp = std::mktime(&tm_);
                d = (int16_t)tmp;
                break;
            }

            return d;
        }

        template<>
        uint16_t get(uint16_t &d) const
        {
            int8_t i8_;
            uint8_t u8_;
            int16_t i16_;
            uint16_t u16_;
            int32_t i32_;
            uint32_t u32_;
            int64_t i64_;
            uint64_t u64_;
            double dble_;
            std::tm tm_;
            time_t tmp;

            switch (it)
            {
            case IT_INT8_T:
                i8_ = boost::any_cast<int8_t>(own);
                d = (uint16_t)i8_;
                break;
            case IT_UINT8_T:
                u8_ = boost::any_cast<uint8_t>(own);
                d = (uint16_t)u8_;
                break;
            case IT_INT16_T:
                i16_ = boost::any_cast<int16_t>(own);
                d = (uint16_t)i16_;
                break;
            case IT_UINT16_T:
                u16_ = boost::any_cast<uint16_t>(own);
                d = (uint16_t)u16_;
                break;
            case IT_INT32_T:
                i32_ = boost::any_cast<int32_t>(own);
                d = (uint16_t)i32_;
                break;
            case IT_UINT32_T:
                u32_ = boost::any_cast<uint32_t>(own);
                d = (uint16_t)u32_;
                break;
            case IT_INT64_T:
                i64_ = boost::any_cast<int64_t>(own);
                d = (uint16_t)i64_;
                break;
            case IT_UINT64_T:
                u64_ = boost::any_cast<uint64_t>(own);
                d = (uint16_t)u64_;
                break;
            case IT_DOUBLE_T:
                dble_ = boost::any_cast<double>(own);
                d = (uint16_t)dble_;
                break;
            case IT_DATETIME:
                tm_ = boost::any_cast<std::tm>(own);
                tmp = std::mktime(&tm_);
                d = (uint16_t)tmp;
                break;
            }

            return d;
        }

        template<>
        int32_t get(int32_t &d) const
        {
            int8_t i8_;
            uint8_t u8_;
            int16_t i16_;
            uint16_t u16_;
            int32_t i32_;
            uint32_t u32_;
            int64_t i64_;
            uint64_t u64_;
            double dble_;
            std::tm tm_;
            time_t tmp;

            switch (it)
            {
            case IT_INT8_T:
                i8_ = boost::any_cast<int8_t>(own);
                d = (int32_t)i8_;
                break;
            case IT_UINT8_T:
                u8_ = boost::any_cast<uint8_t>(own);
                d = (int32_t)u8_;
                break;
            case IT_INT16_T:
                i16_ = boost::any_cast<int16_t>(own);
                d = (int32_t)i16_;
                break;
            case IT_UINT16_T:
                u16_ = boost::any_cast<uint16_t>(own);
                d = (int32_t)u16_;
                break;
            case IT_INT32_T:
                i32_ = boost::any_cast<int32_t>(own);
                d = (int32_t)i32_;
                break;
            case IT_UINT32_T:
                u32_ = boost::any_cast<uint32_t>(own);
                d = (int32_t)u32_;
                break;
            case IT_INT64_T:
                i64_ = boost::any_cast<int64_t>(own);
                d = (int32_t)i64_;
                break;
            case IT_UINT64_T:
                u64_ = boost::any_cast<uint64_t>(own);
                d = (int32_t)u64_;
                break;
            case IT_DOUBLE_T:
                dble_ = boost::any_cast<double>(own);
                d = (int32_t)dble_;
                break;
            case IT_DATETIME:
                tm_ = boost::any_cast<std::tm>(own);
                tmp = std::mktime(&tm_);
                d = (int32_t)tmp;
                break;
            }

            return d;
        }

        template<>
        uint32_t get(uint32_t &d) const
        {
            int8_t i8_;
            uint8_t u8_;
            int16_t i16_;
            uint16_t u16_;
            int32_t i32_;
            uint32_t u32_;
            int64_t i64_;
            uint64_t u64_;
            double dble_;
            std::tm tm_;
            time_t tmp;

            switch (it)
            {
            case IT_INT8_T:
                i8_ = boost::any_cast<int8_t>(own);
                d = (uint32_t)i8_;
                break;
            case IT_UINT8_T:
                u8_ = boost::any_cast<uint8_t>(own);
                d = (uint32_t)u8_;
                break;
            case IT_INT16_T:
                i16_ = boost::any_cast<int16_t>(own);
                d = (uint32_t)i16_;
                break;
            case IT_UINT16_T:
                u16_ = boost::any_cast<uint16_t>(own);
                d = (uint32_t)u16_;
                break;
            case IT_INT32_T:
                i32_ = boost::any_cast<int32_t>(own);
                d = (uint32_t)i32_;
                break;
            case IT_UINT32_T:
                u32_ = boost::any_cast<uint32_t>(own);
                d = (uint32_t)u32_;
                break;
            case IT_INT64_T:
                i64_ = boost::any_cast<int64_t>(own);
                d = (uint32_t)i64_;
                break;
            case IT_UINT64_T:
                u64_ = boost::any_cast<uint64_t>(own);
                d = (uint32_t)u64_;
                break;
            case IT_DOUBLE_T:
                dble_ = boost::any_cast<double>(own);
                d = (uint32_t)dble_;
                break;
            case IT_DATETIME:
                tm_ = boost::any_cast<std::tm>(own);
                tmp = std::mktime(&tm_);
                d = (uint32_t)tmp;
                break;
            }

            return d;
        }

        template<>
        int64_t get(int64_t &d) const
        {
            int8_t i8_;
            uint8_t u8_;
            int16_t i16_;
            uint16_t u16_;
            int32_t i32_;
            uint32_t u32_;
            int64_t i64_;
            uint64_t u64_;
            double dble_;
            std::tm tm_;
            time_t tmp;

            switch (it)
            {
            case IT_INT8_T:
                i8_ = boost::any_cast<int8_t>(own);
                d = (int64_t)i8_;
                break;
            case IT_UINT8_T:
                u8_ = boost::any_cast<uint8_t>(own);
                d = (int64_t)u8_;
                break;
            case IT_INT16_T:
                i16_ = boost::any_cast<int16_t>(own);
                d = (int64_t)i16_;
                break;
            case IT_UINT16_T:
                u16_ = boost::any_cast<uint16_t>(own);
                d = (int64_t)u16_;
                break;
            case IT_INT32_T:
                i32_ = boost::any_cast<int32_t>(own);
                d = (int64_t)i32_;
                break;
            case IT_UINT32_T:
                u32_ = boost::any_cast<uint32_t>(own);
                d = (int64_t)u32_;
                break;
            case IT_INT64_T:
                i64_ = boost::any_cast<int64_t>(own);
                d = (int64_t)i64_;
                break;
            case IT_UINT64_T:
                u64_ = boost::any_cast<uint64_t>(own);
                d = (int64_t)u64_;
                break;
            case IT_DOUBLE_T:
                dble_ = boost::any_cast<double>(own);
                d = (int64_t)dble_;
                break;
            case IT_DATETIME:
                tm_ = boost::any_cast<std::tm>(own);
                tmp = std::mktime(&tm_);
                d = (int64_t)tmp;
                break;
            }

            return d;
        }

        template<>
        uint64_t get(uint64_t &d) const
        {
            int8_t i8_;
            uint8_t u8_;
            int16_t i16_;
            uint16_t u16_;
            int32_t i32_;
            uint32_t u32_;
            int64_t i64_;
            uint64_t u64_;
            double dble_;
            std::tm tm_;
            time_t tmp;

            switch (it)
            {
            case IT_INT8_T:
                i8_ = boost::any_cast<int8_t>(own);
                d = (uint64_t)i8_;
                break;
            case IT_UINT8_T:
                u8_ = boost::any_cast<uint8_t>(own);
                d = (uint64_t)u8_;
                break;
            case IT_INT16_T:
                i16_ = boost::any_cast<int16_t>(own);
                d = (uint64_t)i16_;
                break;
            case IT_UINT16_T:
                u16_ = boost::any_cast<uint16_t>(own);
                d = (uint64_t)u16_;
                break;
            case IT_INT32_T:
                i32_ = boost::any_cast<int32_t>(own);
                d = (uint64_t)i32_;
                break;
            case IT_UINT32_T:
                u32_ = boost::any_cast<uint32_t>(own);
                d = (uint64_t)u32_;
                break;
            case IT_INT64_T:
                i64_ = boost::any_cast<int64_t>(own);
                d = (uint64_t)i64_;
                break;
            case IT_UINT64_T:
                u64_ = boost::any_cast<uint64_t>(own);
                d = (uint64_t)u64_;
                break;
            case IT_DOUBLE_T:
                dble_ = boost::any_cast<double>(own);
                d = (uint64_t)dble_;
                break;
            case IT_DATETIME:
                tm_ = boost::any_cast<std::tm>(own);
                tmp = std::mktime(&tm_);
                d = (uint64_t)tmp;
                break;
            }

            return d;
        }

        template<>
        double get(double &d) const
        {
            int8_t i8_;
            uint8_t u8_;
            int16_t i16_;
            uint16_t u16_;
            int32_t i32_;
            uint32_t u32_;
            int64_t i64_;
            uint64_t u64_;
            double dble_;
            std::tm tm_;
            time_t tmp;

            switch (it)
            {
            case IT_INT8_T:
                i8_ = boost::any_cast<int8_t>(own);
                d = (double)i8_;
                break;
            case IT_UINT8_T:
                u8_ = boost::any_cast<uint8_t>(own);
                d = (double)u8_;
                break;
            case IT_INT16_T:
                i16_ = boost::any_cast<int16_t>(own);
                d = (double)i16_;
                break;
            case IT_UINT16_T:
                u16_ = boost::any_cast<uint16_t>(own);
                d = (double)u16_;
                break;
            case IT_INT32_T:
                i32_ = boost::any_cast<int32_t>(own);
                d = (double)i32_;
                break;
            case IT_UINT32_T:
                u32_ = boost::any_cast<uint32_t>(own);
                d = (double)u32_;
                break;
            case IT_INT64_T:
                i64_ = boost::any_cast<int64_t>(own);
                d = (double)i64_;
                break;
            case IT_UINT64_T:
                u64_ = boost::any_cast<uint64_t>(own);
                d = (double)u64_;
                break;
            case IT_DOUBLE_T:
                dble_ = boost::any_cast<double>(own);
                d = (double)dble_;
                break;
            case IT_DATETIME:
                tm_ = boost::any_cast<std::tm>(own);
                tmp = std::mktime(&tm_);
                d = (double)tmp;
                break;
            }

            return d;
        }

        std::string to_string() const;

    private:
        boost::any own;
        ItemType it;
    };
}

#endif