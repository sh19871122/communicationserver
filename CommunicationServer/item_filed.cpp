#include "item_filed.h"
#include <ctime>
namespace cs {
namespace business {

#ifdef WIN32
#define snprintf sprintf_s
#else
#endif
void ItemFiled::set_io_type( const std::string &t )
{
	try
	{
		int size = boost::lexical_cast<int32_t>(t);
		switch (size)
		{
		case IOTYPE_INPUT:
			io_type = IOTYPE_INPUT;
			break;
		case IOTYPE_OUTPUT:
			io_type = IOTYPE_OUTPUT;
			break;
		case IOTYPE_IOPUT:
			io_type = IOTYPE_IOPUT;
            break;
        default:
            io_type = IOTYPE_ERROR;
		}
	}
	catch(boost::bad_lexical_cast &e)
	{
		LOG(ERROR) << e.what();
		io_type = IOTYPE_IOPUT;
	}
}

std::string ItemFiled::get_str_value( const uint8_t *begin )
{
	std::string row_value((const char *)begin, size);
	static const uint32_t i8  = cs::utility::weak_case_hash( "int8_t");
	static const uint32_t u8  = cs::utility::weak_case_hash("uint8_t");
	static const uint32_t i16 = cs::utility::weak_case_hash( "int16_t");
	static const uint32_t u16 = cs::utility::weak_case_hash("uint16_t"); 
	static const uint32_t i32 = cs::utility::weak_case_hash( "int32_t");
	static const uint32_t u32 = cs::utility::weak_case_hash("uint32_t");
	static const uint32_t i64 = cs::utility::weak_case_hash( "int64_t");
	static const uint32_t u64 = cs::utility::weak_case_hash("uint64_t");
	static const uint32_t str = cs::utility::weak_case_hash("string");
    static const uint32_t dble = cs::utility::weak_case_hash("double");
    static const uint32_t tim = cs::utility::weak_case_hash("time_t");

	std::string result;

	if (typestr.size() && row_value.size() && valid)
	{
		uint32_t hv = cs::utility::weak_case_hash(typestr);
		if (i8 == hv) // char 
		{
			char buf[10] = {0};
			int8_t tmp = row_value.c_str()[0];
			snprintf(buf, sizeof(buf), "%c", tmp);
			result = buf;
		}
		else if (u8 == hv) // uchar
		{
			char buf[10] = {0};
			uint8_t tmp = row_value.c_str()[0];
			snprintf(buf, sizeof(buf), "%u", tmp);
			result = buf;
		}
		else if (i16 == hv) // short
		{
			char buf[10] = {0};
			int16_t tmp = *(int16_t *)row_value.c_str();
			snprintf(buf, sizeof(buf), "%d", tmp);
			result = buf;
		}
		else if (u16 == hv) // ushort
		{
			char buf[10] = {0};
			uint16_t tmp = *(uint16_t *)row_value.c_str();
			snprintf(buf, sizeof(buf), "%u", tmp);
			result = buf;
		}
		else if (i32 == hv) // int
		{
			char buf[64] = {0};
			int32_t tmp = *(int32_t *)row_value.c_str();
			snprintf(buf, sizeof(buf), "%d", tmp);
			result = buf;
		}
		else if (u32 == hv) // uint
		{
			char buf[64] = {0};
			uint32_t tmp = *(uint32_t *)row_value.c_str();
			snprintf(buf, sizeof(buf), "%u", tmp);
			result = buf;
		}
		else if (i64 == hv) // long long
		{
			char buf[128] = {0};
			int64_t tmp = *(int64_t *)row_value.c_str();
			snprintf(buf, sizeof(buf), "%lld", tmp);
			result = buf;
		}
		else if (u64 == hv) // ulong long
		{
			char buf[128] = {0};
			uint64_t tmp = *(uint64_t *)row_value.c_str();
			snprintf(buf, sizeof(buf), "%llu", tmp);
			result = buf;
		}
        else if (dble == hv)
        {
            char buf[128] = {0};
            double tmp = *(double *)row_value.c_str();
            snprintf(buf, sizeof(buf), "%f", tmp);
            result = buf;
        }
        else if (tim == hv)
        {
            // 4 byte deal for time_t
            time_t t = *(uint32_t *)row_value.c_str();
            std::tm tmp = *std::localtime(&t);
            result = std::asctime(&tmp);
        }
		else // str
		{
			result = row_value;
		}
	}

	return result;
}

void ItemFiled::set_size( const std::string len )
{
	try
	{
		size = boost::lexical_cast<int32_t>(len);
	}
	catch(boost::bad_lexical_cast &e)
	{
		LOG(ERROR) << e.what();
		size = 0;
	}
}

}
}