#include "format.h"
#include <gtest/gtest.h>

using namespace cs::utility;

TEST(FormatTest, Format2HexWithCStrTwoArgsInput)
{
    char buf [10] = {0x10, 0x11, 0x00, 0x13, 0x15};
    std::string expect_str = "10110013150000000000";
    std::string rt = Format::Format2Hex(buf, 10);

    EXPECT_STREQ(expect_str.c_str(), rt.c_str());
}

TEST(FormatTest, Format2HexWithFullInput)
{
    char buf [10] = {0x10, 0x11, 0x00, 0x13, 0x15};
    std::string expect_str = "10#11#00#13#\n15#00#00#00#\n00#00#";
    std::string rt = Format::Format2Hex(buf, 10, 1, '#', 4);
    EXPECT_STREQ(expect_str.c_str(), rt.c_str());
}

TEST(FormatTest, Format2HexINT32)
{
    int32_t value = 0x111213;
    std::string rt = Format::Format2Hex(value);
    EXPECT_STREQ("0x00111213", rt.c_str());
}

TEST(FormatTest, Format2HexINT64)
{
    int64_t value = 0x11223344;
    std::string rt = Format::Format2Hex(value);
    EXPECT_STREQ("0x0000000011223344", rt.c_str());
}