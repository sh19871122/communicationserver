#ifndef CS_BUSINESS_BASE_DEAL_V6_H
#define CS_BUSINESS_BASE_DEAL_V6_H
#include "deal_message.h"
#include "protocol.h"
#include <soci.h>
#include <boost/smart_ptr.hpp>
#include <boost/optional.hpp>

namespace cs
{
namespace business
{

    class BaseDealV6 : public DealMessage
    {
    public:
        BaseDealV6(boost::shared_ptr<boost::asio::ip::tcp::socket> client, const std::string &xml_op_path = "EvComm.xml") : 
          DealMessage(client), config_path(xml_op_path)
        {}

        virtual ~BaseDealV6()
        {}

    protected:
        DealMessageState read_request();

        DealMessageState process_request();

        DealMessageState write_response(const uint8_t *begin, size_t len);

    protected:
        //************************************
        // Method:    select_op
        // FullName:  cs::business::BaseDealV6::select_op
        // Access:    protected 
        // Returns:   boost::optional<MyTable>
        // Qualifier:
        // Parameter: const std::string & sql
        //************************************
        boost::optional<MyTable> select_op(const std::string &sql);

        MyTable select_handler(const soci::rowset<soci::row> &rows);

        //************************************
        // Method:    update_op
        // FullName:  cs::business::BaseDealV6::update_op
        // Access:    protected 
        // Returns:   int
        // Qualifier:
        // Parameter: const std::string & sql
        //************************************
        int update_op(const std::string &sql);

        //************************************
        // Method:    delete_op
        // FullName:  cs::business::BaseDealV6::delete_op
        // Access:    protected 
        // Returns:   int
        // Qualifier:
        // Parameter: const std::string & sql
        //************************************
        int delete_op(const std::string &sql);

        //************************************
        // Method:    insert_op
        // FullName:  cs::business::BaseDealV6::insert_op
        // Access:    protected 
        // Returns:   int
        // Qualifier:
        // Parameter: const std::string & sql
        //************************************
        int insert_op(const std::string &sql);

        DealMessageState read_msg_head();

        DealMessageState read_msg_body();

        DealMessageState head_handler();

        DealMessageState body_handler();

        // for testing
    protected:
        cs::MsgHead msg_hdr;
        std::vector<uint8_t> msg_bdy;
        std::string config_path;
    };

}
}

#endif