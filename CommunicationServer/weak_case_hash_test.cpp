#include "weak_case_hash.h"
#include <gtest/gtest.h>

using namespace cs::utility;
TEST(WEEK_CASE_HASH_TEST, input_strcase_not_include_null)
{
    std::string input_a = "aaaa";
    std::string input_b = "AaAa";
    std::string input_c = "aaab";

    uint32_t a = weak_case_hash(input_a);
    uint32_t b = weak_case_hash(input_b);
    uint32_t c = weak_case_hash(input_c);

    EXPECT_TRUE((a == b));

    EXPECT_FALSE((a == c));
}

TEST(WeakCaseHashTest, input_strcase_include_null)
{
    char buf0[10] = {0x10, 0x12, 0x0, 0x41, 0x42, 0x63, 0x0};
    char buf1[10] = {0x10, 0x12, 0x0, 0x61, 0x62, 0x43, 0x0};
    char buf2[10] = {0x10, 0x12, 0x0, 0x41, 0x62, 0x43, 0x1, 0x0};

    std::string b0(buf0, 10);
    std::string b1(buf1, 10);
    std::string b2(buf2, 10);

    EXPECT_TRUE(((b0.size() == b1.size()) && (b0.size() == b2.size()) && (b0.size() == 10)));

    EXPECT_TRUE(weak_case_hash(b0) == weak_case_hash(b1));

    EXPECT_FALSE(weak_case_hash(b0) == weak_case_hash(b2));
}