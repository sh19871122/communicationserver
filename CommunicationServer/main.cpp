#include "server.h"
#include "protocol.h"
#include "weak_case_hash.h"
#include <glog/logging.h>
#include <boost/asio.hpp>

int main(int argc, char *argv[])
{
    using namespace boost::asio;

    google::InitGoogleLogging(argv[0]);
    google::SetStderrLogging(google::GLOG_INFO);
    FLAGS_colorlogtostderr = 1;

    cs::proxy::Server serv;
    serv.start(ip::tcp::endpoint(ip::tcp::v4(), 1207));
    return 0;
}