#ifndef CS_DB_MANAGER_H
#define CS_DB_MANAGER_H

#include <glog/logging.h>
#include <queue>
#include <boost/enable_shared_from_this.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/noncopyable.hpp>
#include <boost/thread.hpp>
#include <boost/thread/once.hpp>
#include <boost/thread/condition.hpp>
#include <boost/bind.hpp>

namespace cs { 
namespace db {
	class Manager;
	typedef boost::shared_ptr<Manager> ManagerPtr;

    // default this class while create 2 threads, one thread for schedule, another
    // for business process
	class Manager : public boost::enable_shared_from_this<Manager>,
		boost::noncopyable
	{
	public:
        Manager() : to_stop(false), starting(false) 
        {}

		void update(boost::shared_ptr<boost::asio::ip::tcp::socket> sockprt)
		{
			boost::mutex::scoped_lock lock(mutex_queue);
			client_queue.push(sockprt);
            // notify blocked thread��blocked get queue items��
			condition_connected.notify_one();
		}

		~Manager()
		{
            stop();
		}

        void start()
        {
            boost::mutex::scoped_lock star_lock(mutex_starstop);
            if (starting)
                return;

            boost::mutex::scoped_lock lock(mutex_queue);
            sched_thread_ptr = boost::shared_ptr<boost::thread>(new boost::thread(boost::bind(sched_func, shared_from_this())));
        }
        
        //************************************
        // Method:    stop
        // FullName:  cs::db::Manager::stop
        // Access:    public 
        // Returns:   void
        // Qualifier: this func will blocked until manager not work
        //************************************
        void stop()
        {
            boost::mutex::scoped_lock lock(mutex_starstop);
            to_stop = true;
            // wake up all waiting with queue
            condition_connected.notify_all();
            // wake up all waiting with switch
            condition_stoped.notify_all();
            // wait to stopped the sched thread exit
            condition_stoped.wait(lock);
            sched_thread_ptr->join();
            starting = false;
        }

        inline bool is_started() const
        {
            boost::mutex::scoped_lock lock(mutex_starstop);
            return starting;

        }

    private:
        static void sched_func(boost::shared_ptr<Manager> own)
        {
            own->mutex_starstop.lock();
            own->starting = true;
            own->to_stop = false;
            own->mutex_starstop.unlock();

            while (!own->to_stop)
            {
                boost::shared_ptr<boost::asio::ip::tcp::socket> tmp;
                {
                    boost::mutex::scoped_lock lock(own->mutex_queue);
                    while (!own->client_queue.size())
                        own->condition_connected.wait(lock);
                    tmp = own->client_queue.front();
                    own->client_queue.pop();
                }

                if (tmp)
                {
                    boost::thread t(boost::bind(business_process, tmp));
                    t.detach();
                }
            }
        }

        static void business_process(boost::shared_ptr<boost::asio::ip::tcp::socket> client);

	private:
        volatile bool to_stop;
        bool starting;
		mutable boost::mutex mutex_queue, mutex_starstop;
		boost::condition condition_connected, condition_stoped;
		std::queue<boost::shared_ptr<boost::asio::ip::tcp::socket> >client_queue;
		boost::shared_ptr<boost::thread> sched_thread_ptr;
	};
}
}

#endif