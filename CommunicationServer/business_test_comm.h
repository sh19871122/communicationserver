#ifndef CS_TEST_BUSINESS_BUSINESS_TEST_COMM_H
#define CS_TEST_BUSINESS_BUSINESS_TEST_COMM_H

#include <string.h>
// structure with mapped test.xml
#pragma pack(8)
struct SelectSt
{
    uint32_t begin_id;
    uint32_t end_id;

    SelectSt()
    {
        memset(this, 0, sizeof(struct SelectSt));
    }
};

struct InsertSt
{
    uint32_t id;
    int8_t stu_name[50];

    InsertSt()
    {
        memset(this, 0, sizeof(struct InsertSt));
    }
};

struct DeleteSt
{
    uint32_t id;

    DeleteSt()
    {
        memset(this, 0, sizeof(struct DeleteSt));
    }
};

struct UpdateSt
{
    int8_t stu_name[50];
    int8_t stu_address[100];
    uint32_t stu_birth;
    uint32_t id;

    UpdateSt()
    {
        memset(this, 0, sizeof(struct UpdateSt));
    }
};

struct SelectStOut
{
    uint32_t id;
    int8_t stu_name[50];
    int8_t stu_address[100];
    uint32_t stu_birth;

    SelectStOut()
    {
        memset(this, 0, sizeof(struct SelectStOut));
    }
};
#pragma pack()

#endif