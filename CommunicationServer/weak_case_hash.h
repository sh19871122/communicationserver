#ifndef CS_UTILITY_WEAK_CASE_HASH_H
#define CS_UTILITY_WEAK_CASE_HASH_H

#include <stdint.h>
#include <algorithm>
#include <string>

namespace cs
{
    namespace utility
    {
        uint32_t weak_case_hash(const std::string &s);

        class WeakCaseHash
        {
        public:
            ::std::size_t operator()(const std::string &rt) const;
        };

        class WeakCaseEqual
        {
        public:
            bool operator()(const std::string &rt, const std::string &lf) const;
        };
    }
}

#endif