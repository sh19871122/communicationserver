#include "base_deal_v6.h"
#include "message_decode.h"
#include "business_test_comm.h"
#include "format.h"

#include <gtest/gtest.h>
#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/typeof/typeof.hpp>

using namespace cs::business;
using namespace boost;
using namespace boost::asio;

class BaseDealV6ForTest : public BaseDealV6
{
public:
    BaseDealV6ForTest(shared_ptr<ip::tcp::socket> client) : 
      BaseDealV6(client, "test.xml") {}

    void set_header(cs::MsgHead &mh)
    {
        memcpy(&this->msg_hdr, &mh, sizeof(cs::MsgHead));
    }

    void set_body(void *begin, size_t len)
    {
        msg_bdy.resize(len);
        memcpy(&msg_bdy[0], begin, len);
    }

    boost::optional<MyTable> select_test()
    {
        boost::optional<MyTable> rt;

        boost::shared_ptr<MessageDecode> md_ptr = MessageDecode::getInstance(config_path);
        boost::shared_ptr<TransportInfo> di_ptr = md_ptr->get_decode_info(cs::utility::Format::Format2Hex(msg_hdr.msg_id));
        if (di_ptr)
        {
            // can use connection_pool
            std::string decode_info = di_ptr->decode(&msg_bdy[0], msg_bdy.size());

            if (decode_info.size())
            {
                switch (di_ptr->get_operator_type())
                {
                case TransportInfo::OPTYPE_SELECT:
                    rt = select_op(decode_info);
                    break;
                }
            }
        }
        return rt;
    }
};

class BaseDealV6Test : public testing::Test
{
};

TEST_F(BaseDealV6Test, select_op_select)
{
    io_service service;
    BaseDealV6ForTest bdv6(shared_ptr<ip::tcp::socket>(new ip::tcp::socket(service)));
    SelectSt ss;
    ss.begin_id = 0;
    ss.end_id = 10;

    cs::MsgHead mh;
    mh.msg_id = 0x00081007;
    mh.data_len = sizeof(SelectSt);

    bdv6.set_header(mh);
    bdv6.set_body(&ss, sizeof(SelectSt));

    boost::optional<MyTable> rt = bdv6.select_test();
    if (!rt)
    {
        EXPECT_EQ(0, 1);
    }
    else
    {
        EXPECT_EQ(9, (*rt).size());
        for (MyTable::iterator it = (*rt).begin(); it != (*rt).end(); it++)
        {
            EXPECT_LT((*it)["id"].get<uint32_t>(), 10);
            EXPECT_GT((*it)["id"].get<uint32_t>(), 0);
        }
    }
}

TEST_F(BaseDealV6Test, select_op_decode)
{

}
