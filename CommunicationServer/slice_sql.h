#ifndef CS_BUSINESS_STRUCT_DECODE_SLICESQL_H
#define CS_BUSINESS_STRUCT_DECODE_SLICESQL_H

#include <string>
namespace cs {
namespace business {

    class SliceSql
    {
    public:
        enum PRIMIT_OP
        {
            PRIMIT_OP_RAW,
            PRIMIT_OP_REP, // replase
        };

    public:
        SliceSql(PRIMIT_OP op, const std::string &r)
            : op(op), raw(r) {}

        SliceSql(const SliceSql &s)
        {
            this->op = s.op;
            this->raw = s.raw;
        }

        PRIMIT_OP op;
        std::string raw;
    };

}
}

#endif