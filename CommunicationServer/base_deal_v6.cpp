#include "base_deal_v6.h"
#include "message_decode.h"
#include "format.h"
#include <exception>
#include <glog/logging.h>
#include <boost/asio.hpp>
#include <boost/date_time.hpp>

namespace cs
{
namespace business
{

    DealMessageState BaseDealV6::read_request()
    {
        DealMessageState dms = head_handler();
        if (dms == DM_STATE_OK)
            dms = body_handler();

        return dms;
    }

    cs::business::DealMessageState BaseDealV6::head_handler()
    {
        DealMessageState dms = read_msg_head();
        switch (dms)
        {
        case DM_STATE_OK:
        case DM_STATE_ERR_CLOSE_CLIENT:
            break;
        default:
            //TODO response error
            break;
        }
        return dms;
    }

    DealMessageState BaseDealV6::read_msg_head()
    {
#define MAX_BODY_SIZE 128*1024*1024
        DealMessageState dms = DM_STATE_OK;
        char *hdr = (char *)&msg_hdr;
        size_t hdr_len = sizeof(cs::MsgHead);
        boost::system::error_code ec;
        boost::asio::read(*client, boost::asio::buffer(hdr, hdr_len), boost::asio::transfer_all(), ec);
        if (ec)
        {
            dms = DM_STATE_ERR_CLOSE_CLIENT;
            LOG(ERROR) << "Client:" << client->remote_endpoint().address() << " Error: " << ec.message();
            return dms;
        }

        if (msg_hdr.data_len >= MAX_BODY_SIZE)
        {
            dms = DM_STATE_ERR_CLOSE_CLIENT;
            std::string fh = cs::utility::Format::Format2Hex((const char *)&msg_hdr, sizeof(MsgHead));
            LOG(ERROR) << "Client:" << client->remote_endpoint().address() << " Error: Packet is too big. MsgHead: " << fh;
        }


        return dms;
    }

    cs::business::DealMessageState BaseDealV6::body_handler()
    {
        DealMessageState dms = read_msg_body();
        switch (dms)
        {
        case DM_STATE_OK:
        case DM_STATE_ERR_CLOSE_CLIENT:
            break;
        default:
            // TODO response client error
            break;
        }

        return dms;
    }

    DealMessageState BaseDealV6::read_msg_body()
    {
        DealMessageState dms = DM_STATE_OK;
        size_t body_len = msg_hdr.data_len;
        msg_bdy.resize(body_len);
        boost::system::error_code ec;
        boost::asio::read(*client, boost::asio::buffer(msg_bdy), boost::asio::transfer_all(), ec);
        if (ec)
        {
            dms = DM_STATE_ERR_CLOSE_CLIENT;
            LOG(ERROR) << "Client:" << client->remote_endpoint().address() << " Error: " << ec.message();
        }

        return dms;
    }

    DealMessageState BaseDealV6::process_request()
    {
        DealMessageState dms = DM_STATE_OK;
        boost::shared_ptr<MessageDecode> md_ptr = MessageDecode::getInstance(config_path);
        boost::shared_ptr<TransportInfo> di_ptr = md_ptr->get_decode_info(cs::utility::Format::Format2Hex(msg_hdr.msg_id));
        if (di_ptr)
        {
            // can use connection_pool
            std::string decode_info = di_ptr->decode(&msg_bdy[0], msg_bdy.size());
            boost::optional<MyTable> mt;

            if (decode_info.size())
            {
                switch (di_ptr->get_operator_type())
                {
                case TransportInfo::OPTYPE_SELECT:
                    mt = select_op(decode_info);
                    if (mt)
                    {
                        std::list<std::string> ls = di_ptr->encode(*mt);
                        LOG(INFO) << " <" << decode_info << "> result cout:" << ls.size();

                        for (auto it = ls.begin(); it != ls.end(); it++)
                        {
                            auto tmp = it;
                            MsgHead mh;
                            mh.msg_index = msg_hdr.msg_index;
                            mh.data_len = it->size();
                            mh.msg_id = this->msg_hdr.msg_id;

                            write_response((const uint8_t *)&mh, sizeof(MsgHead));
                            write_response((const uint8_t *)it->c_str(), it->size());
                        }
                        MsgHead mh;
                        mh.msg_id = this->msg_hdr.msg_id;
                        mh.end_of_file = 1;
                        write_response((const uint8_t *)&mh, sizeof(MsgHead));
                    }
                    break;
                case TransportInfo::OPTYPE_INSERT:
                    insert_op(decode_info);
                    break;
                case TransportInfo::OPTYPE_DELETE:
                    delete_op(decode_info);
                    break;
                case TransportInfo::OPTYPE_UPDATE:
                    update_op(decode_info);
                    break;
                case TransportInfo::OPTYPE_UNKNOWN:
                    break;
                }
            }
            else
            {
                dms = DM_STATE_ERR_READ_BODY;
            }
        }
        else
        {
            dms = DM_STATE_ERR_READ_BODY;
        }

        switch (dms)
        {
        case DM_STATE_ERR_READ_BODY:
            LOG(ERROR) << "Client:" << client->remote_endpoint().address() << " Error: there is not OPERATOR id=" << cs::utility::Format::Format2Hex(msg_hdr.msg_id);
            break;
        }

        return dms;
    }

    //boost::optional<soci::rowset<soci::row> > BaseDealV6::select_op( const std::string &sql )
    boost::optional<MyTable> BaseDealV6::select_op( const std::string &sql )
    {   
        boost::optional<MyTable> rt;
        try
        {
            soci::session sql_session("oracle", "service=55 user=jxidc password=jxidc");

            boost::posix_time::ptime tbegin = boost::posix_time::microsec_clock::universal_time();
            soci::rowset<soci::row> rs(sql_session.prepare << sql);
            boost::posix_time::ptime tend = boost::posix_time::microsec_clock::universal_time();

            rt = select_handler(rs);
        }
        catch (std::exception const &e)
        {
            LOG(ERROR) << client->remote_endpoint().address() << " Error: " << e.what() 
                << " select sql:" << sql;
        }
        return rt;
    }

    MyTable BaseDealV6::select_handler(const soci::rowset<soci::row> &rows)
    {
        using namespace soci;
        using namespace std;;
        using namespace boost;
        using namespace cs;
        using namespace cs::utility;

        MyTable own_table;
        for (rowset<row>::const_iterator cit = rows.begin(); cit != rows.end(); cit++)
        {
            MyRow own_row;
            for (size_t i = 0; i < cit->size(); i++)
            {
                AnyType at;
                const column_properties &props = cit->get_properties(i);
                switch (props.get_data_type())
                {
                case dt_string:
                    (cit->get_indicator(i) == i_null) ? 
                        at.set_value(std::string(""), IT_STRING) :
                        at.set_value(cit->get<string>(i), IT_STRING);
                    own_row[props.get_name()] = at;
                    break;
                case dt_double:
                    (cit->get_indicator(i) == i_null) ?
                        at.set_value(double(0), IT_DOUBLE_T) : 
                        at.set_value(cit->get<double>(i), IT_DOUBLE_T);
                    own_row[props.get_name()] = at;
                    break;
                case dt_integer:
                    (cit->get_indicator(i) == i_null) ?
                        at.set_value(int32_t(0), IT_INT32_T) :
                        at.set_value(cit->get<int>(i), IT_INT32_T);
                    own_row[props.get_name()] = at;
                    break;
                case dt_long_long:
                    (cit->get_indicator(i) == i_null) ?
                        at.set_value(int64_t(0), IT_INT64_T) :
                        at.set_value(cit->get<long long>(i), IT_INT64_T);
                    own_row[props.get_name()] = at;
                    break;
                case dt_unsigned_long_long:
                    (cit->get_indicator(i) == i_null) ?
                        at.set_value(uint64_t(0), IT_UINT64_T) :
                        at.set_value(cit->get<unsigned long long>(i), IT_UINT64_T);
                    own_row[props.get_name()] = at;
                    break;
                case dt_date:
                    (cit->get_indicator(i) == i_null) ?
                        at.set_value(uint64_t(0), IT_DATETIME) :
                        at.set_value(cit->get<std::tm>(i), IT_DATETIME);
                    own_row[props.get_name()] = at;
                    break;
                }
            }
            own_table.push_back(own_row);
        }

        return own_table;
    }

    int BaseDealV6::update_op( const std::string &sql )
    {
        return 0;
    }

    int BaseDealV6::delete_op( const std::string &sql )
    {
        return 0;
    }

    int BaseDealV6::insert_op( const std::string &sql )
    {
        return 0;
    }

    DealMessageState BaseDealV6::write_response(const uint8_t *begin, size_t len)
    {
        using namespace boost::asio;;
        DealMessageState dms = DM_STATE_OK;
        boost::system::error_code ec;
        write(*client, buffer(begin, len), transfer_all(), ec);

        if (ec)
        {
            dms = DM_STATE_ERR_CLOSE_CLIENT;
            LOG(ERROR) << "Client:" << client->remote_endpoint().address() << " Error: " << ec.message();
        }

        return dms;
    }

}
}
