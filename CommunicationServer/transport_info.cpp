#include "transport_info.h"
#include "weak_case_hash.h"
#include "format.h"

#include <map>
#include <string>
#include <glog/logging.h>
#include <boost/typeof/typeof.hpp>

namespace cs {
namespace business {

    TransportInfo::TransportInfo( const TiXmlElement *pelem )
    {
        for (const TiXmlAttribute *attr = pelem->FirstAttribute(); attr; attr = attr->Next())
        {
            uint32_t hash_name = cs::utility::weak_case_hash(attr->Name());
            if (hash_name == cs::utility::weak_case_hash("id"))
            {
                this->set_id(attr->Value());
            }
            else if (hash_name == cs::utility::weak_case_hash("oper"))
            {
                this->set_operator_type(attr->Value());
            }
            else if (hash_name == cs::utility::weak_case_hash("sql"))
            {
                this->set_sql_primitive(attr->Value());
            }
            else if (hash_name == cs::utility::weak_case_hash("recordset"))
            {
                this->set_record(attr->Value());
            }
        }

        for (pelem = pelem->FirstChildElement("filed"); pelem; pelem = pelem->NextSiblingElement("filed"))
        {
            ItemFiled tmp;
            for (const TiXmlAttribute *attr = pelem->FirstAttribute(); attr; attr = attr->Next())
            {
                uint32_t hash_name = cs::utility::weak_case_hash(attr->Name());
                if (hash_name == cs::utility::weak_case_hash("name"))
                    tmp.set_name(attr->Value());
                else if (hash_name == cs::utility::weak_case_hash("type"))
                    tmp.set_typestr(attr->Value());
                else if (hash_name == cs::utility::weak_case_hash("size"))
                    tmp.set_size(attr->Value());
                else if (hash_name == cs::utility::weak_case_hash("valid"))
                    tmp.set_valid(attr->Value());
                else if (hash_name == cs::utility::weak_case_hash("io_type"))
                    tmp.set_io_type(attr->Value());
            }
            if (tmp.get_io_type() & ItemFiled::IOTYPE_INPUT)
                sql_input_items.push_back(tmp);
            if (tmp.get_io_type() & ItemFiled::IOTYPE_OUTPUT)
                sql_output_items.push_back(tmp);
        }

        out_put_size = calculate_size(sql_output_items, 8);
    }

    std::string TransportInfo::decode( const uint8_t *begin, size_t len, int32_t align /*= 8*/ )
    {
        // Linux 32 bit & 64 bit deal with align 8
        if (len % align != 0)
        {
            LOG(WARNING) << "mybe not aligned";
        }

        std::string tmp;
        std::map<std::string, std::string> map_value;
        std::list<ItemFiled>::iterator citer_item, citer_next;

        const uint8_t *str = begin;
        int32_t line_len = 0;
        for (citer_item = sql_input_items.begin(); citer_item != sql_input_items.end(); citer_item++)
        {
            map_value[citer_item->get_name()] = citer_item->get_str_value(str + line_len);

            citer_next = citer_item;
            if (++citer_next != sql_input_items.end())
            {
                line_len = citer_item->get_size();
                line_len += calc_complement_len(citer_next->get_typestr(), line_len, align);
            }
        }

        if ((begin + len) <= str)
        {
            size_t min_len = std::min<size_t>(len, 1023);
            std::string tmp = cs::utility::Format::Format2Hex((const char *)begin, len);
            LOG(ERROR) << "decode data err:" << " ID:" << this->id << 
                " SQL:" << this->sql_prim << " Data Len:" << len << " Binary data:" << tmp;
            return "";
        }

        std::list<SliceSql>::const_iterator cit_sql;
        for (cit_sql = ssql.begin(); cit_sql != ssql.end(); cit_sql++)
        {
            if (cit_sql->op == SliceSql::PRIMIT_OP_RAW)
            {
                tmp.append(cit_sql->raw);
            }
            else
            {
                tmp.append(map_value[cit_sql->raw].c_str());
            }
        }

        return tmp;
    }

    void TransportInfo::set_operator_type( const std::string &type )
    {
        uint32_t type_hash = cs::utility::weak_case_hash(type);
        if (cs::utility::weak_case_hash("select") == type_hash)
        {
            ot = OPTYPE_SELECT;
        }
        else if (cs::utility::weak_case_hash("insert") == type_hash)
        {
            ot = OPTYPE_INSERT;
        }
        else if (cs::utility::weak_case_hash("update") == type_hash)
        {
            ot = OPTYPE_UPDATE;
        }
        else if (cs::utility::weak_case_hash("delete") == type_hash)
        {
            ot = OPTYPE_DELETE;
        }
        else
        {
            ot = OPTYPE_UNKNOWN;
        }
    }

    void TransportInfo::parse_primitive( char split /*= '#'*/ )
    {
        std::string::size_type off_first = std::string::npos;
        std::string::size_type off_pref = 0;
        std::string::size_type off_second = 0;

        do 
        {
            off_first = sql_prim.find(split, off_pref);
            if (off_first != std::string::npos)
            {
                off_second = sql_prim.find(split, off_first + 1);
                if (std::string::npos != off_second)
                {
                    ssql.push_back(SliceSql(SliceSql::PRIMIT_OP_RAW, sql_prim.substr(off_pref, off_first - off_pref)));
                    ssql.push_back(SliceSql(SliceSql::PRIMIT_OP_REP, sql_prim.substr(off_first + 1, off_second - off_first - 1)));
                    off_first = off_second;
                    off_pref = off_first + 1;
                }
                else
                {
                    off_first = off_second;
                }
            }
        } while (off_first != std::string::npos);

        if (off_pref != std::string::npos)
        {
            ssql.push_back(SliceSql(SliceSql::PRIMIT_OP_RAW, sql_prim.substr(off_pref)));
        }
    }

    std::list<std::string> TransportInfo::encode( MyTable &my_table, size_t align)
    {
        std::list<std::string> format_table;

        for (MyTable::iterator my_row = my_table.begin(); 
            my_row != my_table.end(); my_row++)
        {
            std::vector<char> tmp_buf(this->out_put_size);

            size_t now_len = 0;
            for (std::list<ItemFiled>::const_iterator if_cit = sql_output_items.begin();
                if_cit != sql_output_items.end(); if_cit++)
            {
                auto tmp_it = my_row->find(if_cit->get_name());
                if (tmp_it != my_row->end())
                {
                    tmp_it->second.get_value(if_cit->get_typestr(), &tmp_buf[now_len], if_cit->get_size());
                    now_len += if_cit->get_size();
                    auto next = if_cit;
                    if (++next != sql_output_items.end())
                    {
                        now_len += calc_complement_len(next->get_typestr(), now_len, align);
                    }
                }
            }

            format_table.push_back(std::string(tmp_buf.begin(), tmp_buf.end()));
        }
        return format_table;
    }

    size_t TransportInfo::calculate_size(std::list<ItemFiled> &list_item, size_t align_len)
    {
        using namespace cs::utility;
        size_t len = 0;

        for (std::list<ItemFiled>::const_iterator it = list_item.begin();
            it != list_item.end(); it++)
        {
            len = len + calc_complement_len(it->get_typestr(), len, align_len) + it->get_size();
        }

        return len;
    }

    size_t TransportInfo::calc_complement_len( const std::string next_typestr, size_t now_len, size_t align_len )
    {
        using namespace cs::utility;

        if (now_len % align_len == 0)
            return 0;
        
        size_t complete_len = align_len - (now_len % align_len);
        size_t type_len = get_type_len(next_typestr);

        if (complete_len >= type_len)
            complete_len = complete_len % type_len;

        return complete_len;
    }

    size_t TransportInfo::get_type_len( const std::string typestr )
    {
        using namespace cs::utility;

        uint32_t hv = weak_case_hash(typestr);
        if (weak_case_hash("int16_t") == hv ||
            weak_case_hash("uint16_t") == hv)
            return sizeof(int16_t);
        else if (weak_case_hash("int32_t") == hv ||
            weak_case_hash("uint32_t") == hv || 
            weak_case_hash("time_t") == hv)
            return sizeof(int32_t);
        else if (weak_case_hash("int64_t") == hv ||
            weak_case_hash("uint64_t") == hv || 
            weak_case_hash("double") == hv)
            return sizeof(int64_t);
        else 
            return sizeof(int8_t);
    }

}
}
