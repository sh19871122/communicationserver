#include "server.h"
#include "manager.h"
#include <glog/logging.h>

namespace
{
	void listen_thread(boost::asio::io_service &service) 
	{
		boost::system::error_code ec;
		service.run(ec);
		if (ec)
		{
			// log
            LOG(ERROR) << ec.message();
		}
	}
}

#define MEM_FN2(x,y,z)    boost::bind(&self_type::x,this,y,z)

void cs::proxy::Server::start( boost::asio::ip::tcp::endpoint server)
{
    mp->start();
	boost::shared_ptr<io_service::work> dummy_work(new io_service::work(*serviceptr));

	acceptorptr = boost::shared_ptr<ip::tcp::acceptor>(new ip::tcp::acceptor(*serviceptr, server));
	begin_accept();
	error_code ec;
	serviceptr->run(ec);
	// log ec
    if (ec)
    {
        LOG(ERROR) << ec.message();
    }
}

void cs::proxy::Server::stop()
{
	if (!serviceptr->stopped())
		serviceptr->stop();
}

void cs::proxy::Server::begin_accept()
{
	boost::shared_ptr<boost::asio::ip::tcp::socket> sock(new boost::asio::ip::tcp::socket(*serviceptr));
	acceptorptr->async_accept(*sock, MEM_FN2(accept_handler, sock, _1));
}

void cs::proxy::Server::accept_handler(socketprt sp, error_code ec )
{
	if (ec)
	{
		// deal err
		sp->close();
		return;
	}

	// business deal
    mp->update(sp);

	begin_accept();
}
