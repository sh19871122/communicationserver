#ifndef CS_BUSINESS_STRUCT_DECODE_MESSAGE_DECODE_H
#define CS_BUSINESS_STRUCT_DECODE_MESSAGE_DECODE_H

#include "transport_info.h"
#include <stdint.h>
#include <string>
#include <boost/shared_ptr.hpp>
#include <boost/unordered_map.hpp>
#include <boost/thread.hpp>

namespace cs {
namespace business {

    class MessageDecode : public boost::noncopyable
    {
    public:
        boost::shared_ptr<TransportInfo> get_decode_info(const std::string &id) const;

        bool reload_config(const std::string &filename = "");

    public:
        typedef boost::shared_ptr<MessageDecode> MessageDecodePtr;

        static MessageDecodePtr getInstance(const std::string &filename = "");

    private:
        MessageDecode(const std::string &filename);

        bool load_config(const std::string &filename);
        
        static void init(const std::string &filename);

    private:
        typedef boost::unordered_map<uint32_t, boost::shared_ptr<TransportInfo> > DecodeInfoMap;
        DecodeInfoMap dim;
        boost::mutex mutex;
        std::string filename_;

        static boost::once_flag once;
        static MessageDecodePtr instance_ptr;
    };
}
}

#endif


