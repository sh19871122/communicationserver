#include "weak_case_hash.h"
#include <boost/algorithm/string.hpp>


namespace cs
{
    namespace utility
    {
        uint32_t weak_case_hash(const std::string &s)
        {
            uint32_t hash_rt = s.size();
            for (size_t i = 0; i < s.size(); i++)
            {
                uint32_t tmp = (s.c_str()[i] >= 'A' && s.c_str()[i] <= 'Z') ?
                    s.c_str()[i] + 0x20 : s.c_str()[i];

                hash_rt = ((hash_rt << 5) ^ (hash_rt >> 27)) ^ (tmp);
            }
            return hash_rt;
        }

        ::std::size_t WeakCaseHash::operator()( const std::string &rt) const
        {
            return weak_case_hash(rt);
        }


        bool WeakCaseEqual::operator()( const std::string &rt, const std::string &lf ) const
        {
            return boost::iequals(rt, lf);
        }

    }
}