#ifndef CS_BUSINESS_STRUCT_DECODE_ITEM_FILED_H
#define CS_BUSINESS_STRUCT_DECODE_ITEM_FILED_H

#include "weak_case_hash.h"
#include <stdint.h>

#include <string>
#include <glog/logging.h>
#include <boost/lexical_cast.hpp>

namespace cs {
namespace business {

class ItemFiled
{
public:
    enum IOType
    {
        IOTYPE_ERROR = 0,
        IOTYPE_INPUT = 1,
        IOTYPE_OUTPUT = 2,
        IOTYPE_IOPUT = 3
    };

public:
    ItemFiled() : valid(true), size(0), io_type(IOTYPE_IOPUT) {}
        
    inline bool get_valid() const
    {
        return valid;
    }
        
    inline void set_valid(const std::string &v)
    {
        if (cs::utility::weak_case_hash(v) != cs::utility::weak_case_hash("false") && v != "0")
            valid = true;
        else
            valid = false;
    }

    inline int32_t get_size() const
    {
        return size;
    }

    void set_size(const std::string len);
        
    inline std::string get_typestr() const
    {
        return typestr;
    }

    inline void set_typestr(const std::string &t)
    {
        typestr = t;
    }

    inline std::string get_name() const
    {
        return name;
    }

    inline void set_name(const std::string &n) 
    {
        name = n;
        std::transform(name.begin(), name.end(), name.begin(), ::tolower);
    }

    inline IOType get_io_type() const
    {
        return io_type;
    }

    void set_io_type(const std::string &t);

    //************************************
    // Method:    get_str_value
    // FullName:  Item::get_str_value
    // Access:    public 
    // Returns:   std::string, if string's len eq 0 err, other is ok
    // Qualifier: must set the type and row_value, not support dynamic change
    //************************************
    std::string get_str_value(const uint8_t *begin);

private:
    bool valid;
    int32_t  size;
    std::string typestr;
    std::string name;  
    IOType io_type;
};

}
}

#endif