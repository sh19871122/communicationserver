#include "manager.h"
#include "deal_message.h"
#include "base_deal_v6.h"

namespace cs
{
namespace db
{
    void Manager::business_process( boost::shared_ptr<boost::asio::ip::tcp::socket> client )
    {
        boost::shared_ptr<cs::business::DealMessage> dm(new cs::business::BaseDealV6(client));
        dm->begin_deal();
    }
}
}