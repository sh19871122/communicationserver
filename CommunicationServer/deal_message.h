#ifndef CS_BUSINESS_DEAL_MESSAGE_H
#define CS_BUSINESS_DEAL_MESSAGE_H

#include "weak_case_hash.h"
#include "any_type.h"
#include <list>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/asio.hpp>
#include <boost/unordered_map.hpp>
#include <boost/any.hpp>

namespace cs
{
namespace business
{
    typedef boost::unordered_map<std::string, cs::AnyType, cs::utility::WeakCaseHash, cs::utility::WeakCaseEqual> MyRow;
    typedef std::list<MyRow> MyTable;

    enum DealMessageState
    {
        DM_STATE_OK = 0,
        DM_STATE_ERR_READ_HEADER,
        DM_STATE_ERR_READ_BODY,
        DM_STATE_ERR_PROC_SELECT,
        DM_STATE_ERR_PROC_UPDATE,
        DM_STATE_ERR_PROC_DELETE,
        DM_STATE_ERR_PROC_INSERT,
        // eq this will close client connect and release current thread
        DM_STATE_ERR_CLOSE_CLIENT, // read data error,send data error
    };

    class DealMessage : public boost::enable_shared_from_this<DealMessage>
    {
    public:
        DealMessage(boost::shared_ptr<boost::asio::ip::tcp::socket> client)
            : client(client)
        {}

        void begin_deal()
        {
            DealMessageState dms = DM_STATE_OK;
            while (dms != DM_STATE_ERR_CLOSE_CLIENT)
            {
                switch ((dms = read_request()))
                {
                case DM_STATE_OK:
                    dms = process_request();
                    break;
                }
            }
        }

        virtual ~DealMessage()
        {
            boost::system::error_code ec;
            client->close(ec);
        }

    protected:
        virtual DealMessageState read_request() = 0;

        virtual DealMessageState process_request() = 0;

        virtual DealMessageState write_response(const uint8_t *begin, size_t len) = 0;

    protected:
        boost::shared_ptr<boost::asio::ip::tcp::socket> client; 
    };

}
}

#endif