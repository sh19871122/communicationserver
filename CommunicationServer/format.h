#ifndef CS_UTILITY_FORMAT_H
#define CS_UTILITY_FORMAT_H

#include <stdint.h>
#include <string>
#include <sstream>
#include <iomanip>

namespace cs {
    namespace utility {
        class Format
        {
        public:
            //************************************
            // Method:    Format2Hex
            // FullName:  cs::utility::Format::Format2Hex
            // Access:    public static 
            // Returns:   std::string
            // Qualifier:
            // Parameter: const char * begin, will format string
            // Parameter: size_t len, will format len
            // Parameter: int interval, how many characters will insert a interval_symbol
            // Parameter: char interval_symbol, which character insert into format string
            // Parameter: int line_len, how many characters will format with one line
            //************************************
            static std::string Format2Hex(const char *begin, size_t len, 
                int interval = 0, char interval_symbol = ' ', int line_len = 0);

            template<typename T>
            static std::string Format2Hex(T value)
            {
                std::stringstream ss;
                ss << "0x";
                ss << std::hex << std::right << std::setfill('0') << std::uppercase << std::setw(2 * sizeof(T));
                ss << value;

                return ss.str();
            }
        };
    }
}

#endif