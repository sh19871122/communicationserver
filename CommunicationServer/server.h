#ifndef CS_PROXY_SERVER_H
#define CS_PROXY_SERVER_H

#include "manager.h"
#include <list>
#include <boost/enable_shared_from_this.hpp>
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/bind.hpp>
#include <boost/asio.hpp>

namespace cs {
namespace proxy {

	using namespace boost::asio;
	/*
	this class will create a TcpServer (req-rep, pull)
	wait for clients connect and operate database
	*/
	class Server : public boost::noncopyable, 
		boost::enable_shared_from_this<Server>
	{
        typedef Server self_type;
		typedef boost::system::error_code error_code;
		typedef ip::tcp::socket socket;
		typedef boost::shared_ptr<socket> socketprt;
    public:
		Server() : mp(new cs::db::Manager), 
            serviceptr(new io_service) {}

		~Server()
		{
			error_code ec;
			acceptorptr->close(ec);

			stop();
		}

		/************************************************************************/
		/* brief: starting a server with giving endpoint
		/* return: none
		/************************************************************************/
		void start(ip::tcp::endpoint server);

		void stop();

	private:
		void begin_accept();

		void accept_handler(socketprt sp, error_code ec);
	private:
        cs::db::ManagerPtr mp;
		boost::shared_ptr<io_service> serviceptr;
		boost::shared_ptr<ip::tcp::acceptor> acceptorptr;
	};
}
}

#endif