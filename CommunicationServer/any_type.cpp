#include "any_type.h"
#include <ctime>
#include <string>
#include <sstream>
#include <boost/algorithm/string.hpp>

namespace cs
{
    AnyType::~AnyType(void)
    {
    }

    std::string AnyType::to_string() const
    {
        std::stringstream ss;
        std::tm tmp;
        switch (it)
        {
        case IT_UINT8_T:
            ss << get<uint8_t>();
            break;
        case IT_INT8_T:
            ss << get<int8_t>();
            break;
        case IT_INT16_T:
            ss << get<int16_t>();
            break;
        case IT_UINT16_T:
            ss << get<uint16_t>();
            break;
        case IT_INT32_T:
            ss << get<int32_t>();
            break;
        case IT_UINT32_T:
            ss << get<uint32_t>();
            break;
        case IT_INT64_T:
            ss << get<int64_t>();
            break;
        case IT_UINT64_T:
            ss << get<uint64_t>();
            break;
        case IT_STRING:
            ss << get<std::string>();
            break;
        case IT_DOUBLE_T:
            ss << get<double>();
            break;
        case IT_DATETIME:
            tmp = get<std::tm>();
            ss << std::asctime(&tmp);
            break;
        }

        return ss.str();
    }

    bool AnyType::get_value( const std::string &str_type, void *buf, size_t len )
    {
        using namespace boost;
        if (!buf)
            return false;

        bool rt = true;
        if (iequals(str_type, "int8_t"))
        {
            get(*(int8_t *)buf);
        }
        else if (iequals(str_type, "uint8_t"))
        {
            get(*(uint8_t *)buf);
        }
        else if (iequals(str_type, "int16_t"))
        {
            get(*(int16_t *)buf);
        }
        else if (iequals(str_type, "uint16_t"))
        {
            get(*(uint16_t *)buf);
        }
        else if (iequals(str_type, "int32_t"))
        {
            get(*(int32_t *)buf);
        }
        else if (iequals(str_type, "uint32_t"))
        {
            get(*(uint32_t *)buf);
        }
        else if (iequals(str_type, "int64_t"))
        {
            get(*(int64_t *)buf);
        }
        else if (iequals(str_type, "uint64_t"))
        {
            get(*(uint64_t *)buf);
        }
        else if (iequals(str_type, "double"))
        {
            get(*(double *)buf);
        }
        else if (iequals(str_type, "string"))
        {
            std::string tmp;
            get(tmp);
            memcpy(buf, tmp.c_str(), std::min(len, tmp.size()));
        }
        else if (iequals(str_type, "time_t"))
        { 
            get(*(uint32_t *)buf);
        }
        else
        {
            rt = false;
        }
        return rt;
    }

}
