#ifndef CS_BUSINESS_STRUCT_DECODE_TRANSPORT_INFO_H
#define CS_BUSINESS_STRUCT_DECODE_TRANSPORT_INFO_H

#include "item_filed.h"
#include "slice_sql.h"
#include "base_deal_v6.h"

#include <stdint.h>
#include <list>
#include <string>
#include <boost/enable_shared_from_this.hpp>
#include <tinyxml.h>

namespace cs {
namespace business {

    class TransportInfo : public boost::enable_shared_from_this<TransportInfo>
    {
    public: 
        enum OperatorType
        {
            OPTYPE_UNKNOWN = 0,
            OPTYPE_SELECT,
            OPTYPE_INSERT,
            OPTYPE_UPDATE,
            OPTYPE_DELETE
        };

    public: 
        //************************************
        // Method:    DecodeInfo
        // FullName:  cs::business::DecodeInfo::DecodeInfo
        // Access:    public 
        // Returns:   
        // Qualifier:
        // Parameter: const TiXmlElement * pelem(the node name is "OPERATOR")
        //************************************
        TransportInfo(const TiXmlElement *pelem);

        virtual ~TransportInfo() {}

        virtual std::string decode(const uint8_t *begin, size_t len, int32_t align = 8);

        virtual std::list<std::string> encode(MyTable &my_table, size_t align = 8);

        inline std::string get_id() const
        {
            return id;
        }

        inline OperatorType get_operator_type() const
        {
            return ot;
        }

        inline std::string get_record() const
        {
            return record;
        }

        inline std::string get_sql_primitive()
        {
            return sql_prim;
        }

    protected:
        inline void set_id(const std::string &id)
        {
            this->id = id;
            std::transform(this->id.begin(), this->id.end(), this->id.begin(), ::tolower);
        }

        inline void set_operator_type(const std::string &type);

        inline void set_record(const std::string &r)
        {
            record = r;
            std::transform(record.begin(), record.end(), record.begin(), ::tolower);
        }

        inline void set_sql_primitive(const std::string &sql_p)
        {
            sql_prim = sql_p;
            std::transform(sql_prim.begin(), sql_prim.end(), sql_prim.begin(), ::tolower);
            parse_primitive();
        }

        size_t calculate_size(std::list<ItemFiled> &list_item, size_t align_len);

    private:
        void parse_primitive(char split = '#');

        size_t calc_complement_len(const std::string next_typestr, size_t now_len, size_t align_len);

        size_t get_type_len(const std::string typestr);
    private:
        std::string id;
        OperatorType ot;
        std::string record;
        std::string sql_prim;        
        size_t out_put_size;
        std::list<SliceSql> ssql;
        std::list<ItemFiled> sql_input_items;
        std::list<ItemFiled> sql_output_items;


        friend class MessageDecode;
    };
}
}

#endif




