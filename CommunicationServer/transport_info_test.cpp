#include "transport_info.h"
#include "message_decode.h"
#include "business_test_comm.h"
#include <gtest/gtest.h>

using namespace cs::business;

class TransportInfoTest : public testing::Test
{
protected:
    static void SetUpTestCase()
    {
        md_ptr = MessageDecode::getInstance("test.xml");
    }

    static void TearDownTestCase()
    {}

    virtual void SetUp()
    {
        // select
        ti_select = md_ptr->get_decode_info("0x00081007");
        ti_delete = md_ptr->get_decode_info("0x00110E03");
        ti_insert = md_ptr->get_decode_info("0x00110E02");
        ti_update = md_ptr->get_decode_info("0x000F1008");
    }

    virtual void TearDown()
    {}

    static boost::shared_ptr<MessageDecode> md_ptr;

    boost::shared_ptr<TransportInfo> ti_select, ti_update, ti_delete, ti_insert;
};

boost::shared_ptr<MessageDecode> TransportInfoTest::md_ptr = boost::shared_ptr<MessageDecode>((MessageDecode *)NULL);

TEST_F(TransportInfoTest, DECODE_SELECT_TEST)
{
    SelectSt ss;
    ss.begin_id = 0;
    ss.end_id = 10;

    // select * from soci_test where id > #begin_id# and id < #end_id#
    std::string expect_rt = "select * from soci_test where id > 0 and id < 10";
    std::string rt = ti_select->decode((const uint8_t *)&ss, sizeof(SelectSt));
    
    EXPECT_EQ(expect_rt, rt);
}

TEST_F(TransportInfoTest, DECODE_UPDATE_TEST)
{
    UpdateSt us;
    us.id = 1127;
    std::string name("han.shi");
    memcpy(us.stu_name, name.c_str(), name.size());
    std::string addr("hello world");
    memcpy(us.stu_address , addr.c_str(), addr.size());
    us.stu_birth = 1387768976;

    // update soci_test set stu_name = '#stu_name#', stu_address = '#stu_address#', stu_birth = '#stu_birth#' where id = #id#
    std::string expect_rt = "update soci_test set stu_name = 'han.shi', stu_address = 'hello world', stu_birth = 'to_date(''2013-12-16 16:34:15'',''yyyy-mm-dd hh24:mi:ss')' where id = 1127";
    std::string rt = ti_update->decode((const uint8_t *)&us, sizeof(UpdateSt));
}

TEST_F(TransportInfoTest, DECODE_DELETE_TEST)
{
    DeleteSt ds;
    ds.id = 1024;

    // delete from soci_test where id = #id#
    std::string expect_rt = "delete from soci_test where id = 1024";
    std::string rt = ti_delete->decode((const uint8_t *)&ds, sizeof(DeleteSt));

    EXPECT_EQ(expect_rt, rt);
}

TEST_F(TransportInfoTest, DEOCDE_INSERT_TEST)
{
    InsertSt is;
    is.id = 1000000;
    std::string name("Hallelujah");
    memcpy(is.stu_name, name.c_str(), name.size());

    // insert into soci_test(id, stu_name) values(#id#, '#stu_name#')
    std::string expect_rt = "insert into soci_test(id, stu_name) values(1000000, 'Hallelujah')";
    std::string rt = ti_insert->decode((const uint8_t *)&is, sizeof(InsertSt));
    EXPECT_EQ(expect_rt, rt);
}

TEST_F(TransportInfoTest, GET_ID)
{
    std::string select_id("0x00081007");
    EXPECT_STRCASEEQ(select_id.c_str(), ti_select->get_id().c_str());

    std::string update_id("0x000F1008"); 
    EXPECT_STRCASEEQ(update_id.c_str(), ti_update->get_id().c_str());

    std::string delete_id("0x00110E03");
    EXPECT_STRCASEEQ(delete_id.c_str(), ti_delete->get_id().c_str());

    std::string insert_id("0x00110E02");
    EXPECT_STRCASEEQ(insert_id.c_str(), ti_insert->get_id().c_str());
}

TEST_F(TransportInfoTest, ENCODE_INSERT_TEST)
{

}

TEST_F(TransportInfoTest, ENCODE_UPDATE_TEST)
{

}

TEST_F(TransportInfoTest, ENCODE_DELETE_TEST)
{

}

TEST_F(TransportInfoTest, ENCODE_SELECT_TEST)
{

}
