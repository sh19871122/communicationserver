#ifndef CS_PROXY_PROXY_H
#define CS_PROXY_PROXY_H

#include <boost/enable_shared_from_this.hpp>
#include <boost/noncopyable.hpp>
#include <boost/smart_ptr.hpp>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/system/error_code.hpp>

namespace cs {
namespace proxy {
	using namespace boost::asio;
	class Proxy : public boost::noncopyable,
		boost::enable_shared_from_this<Proxy>
	{
	public:
		typedef boost::shared_ptr<Proxy> ProxyPtr;
		typedef boost::system::error_code err_code;
		typedef boost::asio::ip::tcp::endpoint endpoint;

		static ProxyPtr start();
		void stop();
		bool is_started() const;
	};
}
}
#endif