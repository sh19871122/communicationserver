#ifndef CS_PROTOCOL_H
#define CS_PROTOCOL_H
#include <stdint.h>
#include <string.h>

namespace cs
{
#pragma pack(8)
    typedef struct GHCA_IPADDR
    {
        uint8_t ipaddr[16];
        uint64_t flag;
    }GHCA_IPADDR;

    typedef struct MsgHead
    {
        GHCA_IPADDR peer_addr;
        GHCA_IPADDR send_addr;

        uint32_t peer_port;
        uint32_t send_port;
        
        uint32_t send_type; 
        uint32_t msg_index; // unique message identify
        uint32_t msg_id;    // the name of message
        uint32_t data;   // a specify field
        uint32_t data_len;  // payload data length
        uint32_t end_of_file;  // the flag of trans end, must set flag 1

        uint32_t data_type;
        uint32_t socket_num;
        uint8_t param[32]; // redundancy field
        MsgHead()
        {
            memset(this, 0, sizeof(struct MsgHead));
        }
    }MsgHead;
#pragma pack()
}

#endif