#include "item_filed.h"
#include <gtest/gtest.h>
#include <string>

using namespace cs::business;

class ItemFiledTest : public testing::Test
{
};

TEST_F(ItemFiledTest, item_set_get_name)
{
    ItemFiled item_f;
    item_f.set_name("CardId");
    
    EXPECT_STREQ("cardid", item_f.get_name().c_str());
}

TEST_F(ItemFiledTest, item_set_get_type)
{
    ItemFiled item_f;
    item_f.set_typestr("int8_t");
    EXPECT_STREQ("int8_t", item_f.get_typestr().c_str());
    
    item_f.set_typestr("uint8_t");
    EXPECT_STREQ("uint8_t", item_f.get_typestr().c_str());

    item_f.set_typestr("int16_t");
    EXPECT_STREQ("int16_t", item_f.get_typestr().c_str());

    item_f.set_typestr("uint16_t");
    EXPECT_STREQ("uint16_t", item_f.get_typestr().c_str());

    item_f.set_typestr("int32_t");
    EXPECT_STREQ("int32_t", item_f.get_typestr().c_str());

    item_f.set_typestr("uint32_t");
    EXPECT_STREQ("uint32_t", item_f.get_typestr().c_str());

    item_f.set_typestr("int64_t");
    EXPECT_STREQ("int64_t", item_f.get_typestr().c_str());

    item_f.set_typestr("uint64_t");
    EXPECT_STREQ("uint64_t", item_f.get_typestr().c_str());

    item_f.set_typestr("string");
    EXPECT_STREQ("string", item_f.get_typestr().c_str());
}

TEST_F(ItemFiledTest, item_set_get_size)
{
    ItemFiled item_f;

    item_f.set_size("4");
    EXPECT_EQ(4, item_f.get_size());

    item_f.set_size("hello world"); // trans false
    EXPECT_EQ(0, item_f.get_size());

    item_f.set_size("2000000000");
    EXPECT_EQ(2000000000, item_f.get_size());

    item_f.set_size("99999999999999"); // to big trans false
    EXPECT_NE(99999999999999, item_f.get_size());
}

TEST_F(ItemFiledTest, item_set_get_valid)
{
    ItemFiled item_f;
    item_f.set_valid("0");
    EXPECT_FALSE(item_f.get_valid());

    item_f.set_valid("123");
    EXPECT_TRUE(item_f.get_valid());

    item_f.set_valid("false");
    EXPECT_FALSE(item_f.get_valid());

    item_f.set_valid("true");
    EXPECT_TRUE(item_f.get_valid());
}

TEST_F(ItemFiledTest, item_set_get_io_type)
{
    ItemFiled item_f;
    item_f.set_io_type("0"); // error
    EXPECT_TRUE(ItemFiled::IOTYPE_ERROR == item_f.get_io_type());

    item_f.set_io_type("1"); // input
    EXPECT_TRUE(ItemFiled::IOTYPE_INPUT == item_f.get_io_type());

    item_f.set_io_type("2"); // output
    EXPECT_TRUE(ItemFiled::IOTYPE_OUTPUT == item_f.get_io_type());

    item_f.set_io_type("3"); // ioput
    EXPECT_TRUE(ItemFiled::IOTYPE_IOPUT == item_f.get_io_type());

    item_f.set_io_type("4"); // error
    EXPECT_TRUE(ItemFiled::IOTYPE_ERROR == item_f.get_io_type());
}

TEST_F(ItemFiledTest, item_get_str_value)
{
    // little endian
    uint8_t buf[] = {0x41, 0x80, 0x81, 0xff, 0xff, 0xff, 0xff, 0x61, 0x62, 0x63, 0x64, 0x0};
    ItemFiled i8, u8, i16, u16, u32, str;
    i8.set_typestr("INT8_T");
    i8.set_size("1");
    EXPECT_STREQ("A", i8.get_str_value(buf).c_str());

    u8.set_typestr("uint8_t");
    u8.set_size("1");
    EXPECT_STREQ("65", u8.get_str_value(buf).c_str());

    i16.set_size("2");
    i16.set_typestr("int16_t");
    EXPECT_STREQ("-32384", i16.get_str_value(buf + 1).c_str());

    u16.set_size("2");
    u16.set_typestr("UINT16_T");
    EXPECT_STREQ("33152", u16.get_str_value(buf + 1).c_str());

    u32.set_size("4");
    u32.set_typestr("uint32_t");
    EXPECT_STREQ("4294967295", u32.get_str_value(buf + 3).c_str());

    str.set_typestr("string");
    str.set_size("4");
    EXPECT_STREQ("abcd", str.get_str_value(buf + 7).c_str());
}
