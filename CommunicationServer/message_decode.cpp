#include "message_decode.h"
#include "weak_case_hash.h"
#include <stdexcept>
#include <boost/thread/once.hpp>

namespace cs {
namespace business {

    boost::shared_ptr<TransportInfo> MessageDecode::get_decode_info(const std::string &id) const
    {
        uint32_t key = cs::utility::weak_case_hash(id);
        DecodeInfoMap::const_iterator it = dim.find(key);

        return (it != dim.end()) ? it->second : boost::shared_ptr<TransportInfo>((TransportInfo *)NULL);
    }

    bool MessageDecode::reload_config(const std::string &filename)
    {
       boost::mutex::scoped_lock sl(mutex);
       if (filename.size())
           return load_config(filename);
       else
           return load_config(filename_);
    }

    MessageDecode::MessageDecodePtr MessageDecode::getInstance(const std::string &filename)
    {
        boost::call_once(once, boost::bind(MessageDecode::init, filename));

        return instance_ptr;
    }

    boost::once_flag MessageDecode::once = BOOST_ONCE_INIT;

    // if new MessageDecode will throw exception
    void MessageDecode::init(const std::string &filename)
    {
        instance_ptr = boost::shared_ptr<MessageDecode>(new MessageDecode(filename));
        if (!instance_ptr)
            throw std::runtime_error("create MessageDecode error");
    }

    MessageDecode::MessageDecode(const std::string &filename)
    {
        this->filename_ = filename;
        // load xml config
        load_config(filename);
    }

    bool MessageDecode::load_config( const std::string &filename )
    {
        using namespace cs::utility;
        TiXmlDocument xd;
        if (!xd.LoadFile(filename.c_str()))
            return false;

        TiXmlElement *e = xd.FirstChildElement("root");
        e = e->FirstChildElement("MSG");
        e = e->FirstChildElement("OPERATOR");
        for (; e; e = e->NextSiblingElement("OPERATOR"))
        {
            TiXmlAttribute *attr = e->FirstAttribute();
            for (; attr; attr = attr->Next())
            {
                if (weak_case_hash("id") == weak_case_hash(attr->Name()))
                {
                    dim[weak_case_hash(attr->Value())] = boost::shared_ptr<TransportInfo>(new TransportInfo(e));
                    break;
                }
            }
        }
        return true;
    }

    MessageDecode::MessageDecodePtr MessageDecode::instance_ptr = boost::shared_ptr<MessageDecode>((MessageDecode *)NULL);

}
}
