#ifndef CS_DB_OPERATE_DB_H
#define CS_DB_OPERATE_DB_H

#include <boost/enable_shared_from_this.hpp>

template<typename T, typename R>
class OperateDB : 
	public boost::enable_shared_from_this<OperateDB>
{
public:
	virtual R select(T &) = 0;
	virtual R insert(T &) = 0;
	virtual R update(T &) = 0;
	virtual R delet(T &)  = 0;

	virtual void async_insert(T &) = 0;
	virtual void async_update(T &) = 0;
	virtual void async_delet(T &)  = 0;
public:
	virtual ~OperateDB() {}
};

#endif